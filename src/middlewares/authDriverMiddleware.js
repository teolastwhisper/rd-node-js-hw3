const jwt = require('jsonwebtoken');

const authDriverMiddleware = (req, res, next) => {
    const {role} = req.user
    if(!role){
       return res.status(400).json({message:'Wrong Authentification header'});
    }
    if(role!='DRIVER'){
      return  res.status(400).json({message: 'Only driver can have access'});
    }
    next()

};

module.exports = {
  authDriverMiddleware,
};
