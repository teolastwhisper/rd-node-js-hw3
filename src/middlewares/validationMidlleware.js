const Joi = require("joi");

const enumValidator = Joi.object().keys({
  type: Joi.string().valid("DRIVER", "SHIPPER"),
});
const registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
      .required(),
    password: Joi.string().min(3).max(20).required(),
    role : Joi.string().valid('DRIVER','SHIPPER'),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};
const addTruckValidator = async (req, res, next) => {
  console.log( await req.body);
  const schema = Joi.object({
    type: Joi.string()
       .valid('SPRINTER','SMALL STRAIGHT','LARGE STRAIGHT')

  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};
module.exports = {
  registrationValidator,
  addTruckValidator,
};
