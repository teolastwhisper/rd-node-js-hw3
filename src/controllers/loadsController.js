const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {
    asyncWrapper,
  } = require('../utils/apiUtils');
  const {
    InvalidRequestError,
  } = require('../utils/errors');

  const {
    addLoadForUser,
    getUsersLoads,
    postLoadForUser
  } = require('../services/loadsService');
  router.post('/',  asyncWrapper(async (req, res) => {
    const {userId,role} = req.user;
    if(role!='SHIPPER'){
      throw new InvalidRequestError('You can only have acces with shipper role');
    }
  
    const load = await  addLoadForUser(userId, req.body);
  
    res.json({message: 'Load created successfully'});
  }));
  router.post('/:id/post',  asyncWrapper(async (req, res) => {
    const {userId,role} = req.user;
    const {id} = req.params
    if(role!='SHIPPER'){
      throw new InvalidRequestError('You can only have acces with shipper role');
    }
  
    const load = await  postLoadForUser(id);
  
    res.json({driver_found:load, message: 'Load created successfully'});
  }));
  router.get('/',  asyncWrapper(async (req, res) => {
    const {userId,role} = req.user;
  
    const loads = await getUsersLoads(userId, role, req.body);
  
    res.json({loads});
  }));
module.exports = {
    loadsRouter: router,
  };
  