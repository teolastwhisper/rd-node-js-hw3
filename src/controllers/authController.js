const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {registration, login} = require('../services/authService');

const {asyncWrapper} = require('../utils/apiUtils');
const {
  registrationValidator,
} = require('../middlewares/validationMidlleware');

router.post(
    '/register',
    registrationValidator,
    asyncWrapper(async (req, res) => {
      const {role,email, password} = req.body;

      await registration({email,role, password});

      res.json({message: 'Account created successfully!'});
    }),
);

router.post(
    '/login',
    asyncWrapper(async (req, res) => {
      const {email, password} = req.body;
      console.log(email)
      const jwt_token = await login({email, password});

      res.json({jwt_token, message: 'Logged in successfully!'});
    }),
);

module.exports = {
  authRouter: router,
};
