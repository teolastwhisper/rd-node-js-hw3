const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {
  getTrucksByUserId,
  getTruckByIdForUser,
  addTruckToUser,
  updateTruckByIdForUser,
  assignTruckByIdForUser,
  deleteTruckByIdForUser,
} = require('../services/trucksService');
const {
  addTruckValidator,
} = require('../middlewares/validationMidlleware');

const {
  asyncWrapper,
} = require('../utils/apiUtils');
const {
  InvalidRequestError,
} = require('../utils/errors');

router.get('/',  asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const trucks = await getTrucksByUserId(userId, req.body);

  res.json({trucks});
}));

router.get('/:id',asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  const truck = await getTruckByIdForUser(id, userId);

  if (!truck) {
    throw new InvalidRequestError('No truck with such id found!');
  }

  res.json({truck});
}));

router.post('/',  addTruckValidator, asyncWrapper(async (req, res) => {
  const {userId, role} = req.user;
  
  await addTruckToUser(userId, req.body);
  

  res.json({message: 'Truck created successfully'});
}));

router.put('/:id', addTruckValidator, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const {type} = req.body;
  console.log(id);
  await updateTruckByIdForUser(id, userId, type);

  res.json({message: 'Truck updated successfully'});
}));

router.post('/:id/assign', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
   await assignTruckByIdForUser(id, userId);

  res.json({message: 'Truck updated successfully'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await  deleteTruckByIdForUser(id, userId);

  res.json({message: 'Note deleted successfully'});
}));

module.exports = {
  trucksRouter: router,
};
