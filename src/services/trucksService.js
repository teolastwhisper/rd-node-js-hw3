const {Truck} = require('../models/truckModel');

const getTrucksByUserId = async (userId, truckPayload) => {
  const {limit, offset} = truckPayload;
  const trucks = await Truck.find({created_by: userId})
      .limit(parseInt(limit, 10))
      .skip(parseInt(offset, 10))
      .where('deleted')
      .equals(false);
  return trucks;
};

const getTruckByIdForUser = async (truckId, userId) => {
  console.log(truckId)
  const truck = await Truck.findOne({_id: truckId, created_by : userId}).where('deleted').equals(false);
  return truck;
};

const addTruckToUser = async (userId, truckPayload) => {
  const truck = new Truck({...truckPayload, created_by: userId});
  await truck.save();
};

const updateTruckByIdForUser = async (truckId, userId, type) => {
  await Truck.findOneAndUpdate(
      {_id: truckId, created_by : userId},
      {$set: {type: type}},
  ).where('deleted').equals(false);
};
const assignTruckByIdForUser = async (truckId, userId) => {
  await Truck.findOneAndUpdate(
      {_id: truckId, created_by: userId},
      {$set: {assigned_to: userId}},
  ).where('deleted').equals(false);

};
const deleteTruckByIdForUser = async (truckId, userId) => {
  await Truck.findOneAndUpdate( {_id: truckId, created_by : userId},
    {$set: {deleted: true}},
);
};

module.exports = {
  getTrucksByUserId,
  getTruckByIdForUser,
  addTruckToUser,
  updateTruckByIdForUser,
  assignTruckByIdForUser,
  deleteTruckByIdForUser,
};
