const { Load } = require("../models/loadModel");
const {Truck} = require ('../models/truckModel')
const {
    InvalidRequestError,
  } = require('../utils/errors');

TruckTypes = {
    'SPRINTER':{
        w:300,
        l:250,
        h:170
    },
    'SMALL STRAIGHT':{
        w:500,
        l:250,
        h:170
    },
    'LARGE STRAIGHT':{
        w:700,
        l:350,
        h:200
    }

}
const addLoadForUser = async (userId, loadPayload) => {
  const load = new Load({ ...loadPayload, created_by: userId });
  await load.save();
};

const getUsersLoads = async (userId,role, loadPayload) => {
    const {limit, offset} = loadPayload
    if(role==='SHIPPER'){
        const loads = await Load.find({created_by: userId}).
        limit(parseInt(limit, 10))
        .skip(parseInt(offset, 10))
       
        return loads;
    }
    else{
        const loads = await Load.find({status: 'ASSIGNED'||'SHIPPED'}).limit(parseInt(limit, 10))
        .skip(parseInt(offset, 10))
        .where('deleted')
        .equals(false);
        return loads;
    }
     
};
const postLoadForUser = async (loadId)=>{

     const load = await Load.find({_id: loadId})
     if(load[0].status === 'POSTED'){
        throw new InvalidRequestError('Load is already posted');
     }
     await Load.findOneAndUpdate({_id: loadId}, {$set: {status: 'POSTED'}})
    const trucks = await Truck.find({status:'IS'})
    const loadP = load[0]
    const dimensions =  loadP.dimensions;
    let status = loadP.status
    let state = loadP.state
    for(let truck of trucks){
        let truckDimensions =TruckTypes[truck.type]
        console.log(truckDimensions)
        if(truckDimensions.w>=dimensions.width && truckDimensions.h >= dimensions.height && truckDimensions.l >= dimensions.length){
            truck.status = 'OL';
            await Load.findOneAndUpdate({_id: loadId}, {$set: {status: 'ASSIGNED'}, $set: {state:'En route to Pick Up'}})
            truck.save()
            return true
        }
    }
    return false
}

module.exports = {
  addLoadForUser,
  getUsersLoads,
  postLoadForUser
};
