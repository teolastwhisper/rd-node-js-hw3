const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUserById = async (userId)=>{
  const user = await User.findOne({_id: userId});
  return user;
};
const deleteUserById = async (userId)=>{
  await User.findOneAndUpdate({_id: userId, }, {$set: {deleted: true}});
};
const changeUserPass = async (userId, newPassword, oldPassword)=>{
  const user = await User.findById({_id: userId});
  if (!user) {
    throw new Error('No user was found');
  }
  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error('old Password is incorrect, try again');
  }

  await user.update({password: await bcrypt.hash(newPassword, 10)});
};

module.exports ={
  deleteUserById,
  getUserById,
  changeUserPass,

};
