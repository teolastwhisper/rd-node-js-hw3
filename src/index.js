const express = require('express');
// const path = require('path');
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();
const app = express();
const {trucksRouter} = require('./controllers/trucksController');
const {authRouter} = require('./controllers/authController');
const {usersRouter} = require('./controllers/usersController');
const { loadsRouter } = require('./controllers/loadsController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {authDriverMiddleware} = require('./middlewares/authDriverMiddleware')
const {NodeCourseError} = require('./utils/errors');
const PORT = process.env.PORT;
const DB_URL = process.env.DB_URL;
app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/users', [authMiddleware], usersRouter );
app.use('/api/auth', authRouter);
app.use('/api/loads', [authMiddleware], loadsRouter);
app.use('/api/trucks', [authMiddleware], [authDriverMiddleware], trucksRouter);

app.use((req, res, next) => {
  res.status(404).json({message: 'Not found'});
});

app.use((err, req, res, next) => {
  if (err instanceof NodeCourseError) {
    return res.status(err.status).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect( DB_URL, {
      useNewUrlParser: true, useUnifiedTopology: true,
    });

    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
